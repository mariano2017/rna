import numpy as np

class MLP:

    def __init__(self, epochs, eta=0.1):
        self.learning_rate = eta
        self.epochs = epochs
        self.training_set = []
        self.test_set = []
        self.n_layers = 0
        self.hiddenLayer = 0  # -> quantidade neurônios camada oculta
        self.outLayer = 0  # -> quantidade neurônios camada de saida
        self.n_attributtes = 0  # -> quantidade de variávies/atributos
        self.rmse = 0;
        self.training_errors = []
        self.test_errors = []
        self.allw = []
        self.allm = []
        self.initWeigths()

    def initData(self, n_hiddenLayer=3, n_outLayer=2, n_layers=1):
        self.n_layers = n_layers
        self.hiddenLayer = n_hiddenLayer  # -> quantidade neurônios camada oculta
        self.outLayer = n_outLayer  # -> quantidade neurônios camada de saida

    def initSamples(self, training_set, test_set, outputs):
        self.training_set = training_set
        self.test_set = test_set
        self.n_attributtes = len(self.training_set[0]-outputs)  # -> quantidade de variávies/atributos

    def getParam(self):
        return self.training_errors, self.test_errors, self.allm

    def initWeigths(self):
        # -> inicializa os pesos de forma randomica
        self.weights_hidden = np.random.random((self.hiddenLayer, self.n_attributtes))
        self.weights_output = np.random.random((self.outLayer, self.hiddenLayer))

    def predict(self, x, option=1):
        if(option == 0):
            return self.step(x)
        elif (option == 1):
            return 1.0 / (1.0 + np.exp(-x))
        elif (option == 2):
            return (1 - np.exp(float(-x)))/(1 + np.exp(float(-x)))
        else:
            return x

    def derivate(self, y, option=1):
        if (option == 1):
            return y * (1.0 - y)
        if (option == 2):
            return (1/2)*(1 - y*y)
        else:
            return 1

    def activation(self, inputs, weigths, option=1):
        u = np.dot(inputs, weigths)
        u = np.array(u, dtype=np.float64)
        y = []
        for i in u:
            y.append(self.predict(i, option))
        return np.array(y)

    def getWeigths(self):
        return self.weights_hidden, self.weights_output

    def step(self, outputs, option):
        v = -1 if(option == 2) else 0
        pos = np.amax(outputs)
        return np.where(outputs == pos, 1, v)

    def feedforward(self, x, d, option=1):
        h = self.activation(x, self.weights_hidden.T)
        y = self.activation(h, self.weights_output.T, option=option)
        return y, h

    def backpropagation(self, error, h, y, x, option=1):
        y_delta = np.multiply(error, self.derivate(y, option))

        h_error = np.dot(y_delta, self.weights_output)
        h_delta = np.multiply(h_error, self.derivate(h))

        self.weights_output = self.adjustWeights(h, y_delta, self.weights_output)
        self.weights_hidden = self.adjustWeights(x, h_delta, self.weights_hidden)

    def adjustWeights(self, x, delta, w):
        error = np.ones((x.shape[0], delta.shape[0]), dtype=int) * np.asarray(delta)
        update = self.learning_rate * error.T * x
        return w + update

    def training(self, n_outputs, option=1):
        trainingErrors = []
        epochError = 0
        for i in range(self.epochs):
            errors = np.array([])
            for j in self.training_set:
                x, d = j[0:self.n_attributtes], j[-n_outputs:len(j)]
                y, h = self.feedforward(x, d, option)
                error = d - y
                self.backpropagation(error, h, y, x, option)

                if (option == 3):
                    errors = np.append(errors, error * error)

            if (option == 3):
                epochError = errors.mean()
                trainingErrors.append(epochError)

            np.random.shuffle(self.training_set)

        self.allw.append(self.weights_hidden)
        self.training_errors.append(trainingErrors)


    def test(self, outputs, option=1):
        testerrors = []
        hits = 0
        for i in self.test_set:
            x, d = i[0:self.n_attributtes], i[-outputs:len(i)]
            h = self.activation(x, self.weights_hidden.T)
            g = self.activation(h, self.weights_output.T, option)
            y = self.step(g, option)
            error = d - y

            testerrors.append(error*error)
            if np.array_equal(y, d):
                hits += 1

        self.test_errors.append(np.mean(testerrors))
        return hits/len(self.test_set)