import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D as a3d

def plotError(error):
    plt.plot(range(1, error.shape[0] + 1), error)
    plt.xlabel('Iterações')
    plt.ylabel('Erros')
    plt.show()

def plotData2d(dataset, w):
    y =[]
    for i in dataset:
        y.append(np.dot(w.T, i[0:len(i)]))

    plt.scatter(dataset[:, 1], dataset[:, -1], s=3)
    plt.scatter(dataset[:, 1], y, color='r')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()


def teste(dataset):
    plt.scatter(dataset[:, 1], dataset[:, -1], s=3)
    plt.show()
