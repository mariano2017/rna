from sklearn.model_selection import KFold
import numpy as np
import MLP
import Util as ut
from sklearn.model_selection import train_test_split
import Parameterize as pr

class GridSearch():

    def __init__(self, data, realizations, epochs, rate, option, n_class, k, nrangemin=0, nrangemax=10):

        self.learning_rate = rate
        self.epochs = epochs
        self.k = k
        self.pr = data
        self.samples = data.getSamples()
        self.k_size = 0.2
        self.n_attributtes = self.samples.shape[1] - n_class
        self.option = option
        self.nrangemin = nrangemin
        self.nrangemax = nrangemax
        self.n_class = n_class
        self.training_errors = []
        self.test_errors = []
        self.allw = []
        self.allm = []
        self.realizations = realizations

    def crossvalidation(self, dataset):
        kf = KFold(n_splits=self.k)

        acc = []
        for i in range(self.nrangemin, self.nrangemax):

            hitratings = []
            for train_index, test_index in kf.split(dataset):
                mlp = MLP.multilayer_perceptron(self.epochs, self.learning_rate)
                train, test = dataset[train_index], dataset[test_index]
                mlp.initData(i, self.n_class)
                mlp.initSamples(train, test, self.n_class)
                mlp.initWeigths()
                mlp.training(self.n_class, self.option)
                hitratings.append(np.array(mlp.test(self.n_class, self.option)))
                del mlp

            acc.append((np.array(hitratings).mean()))

        amax = max(acc)

        return self.nrangemin + acc.index(amax)
    
    def execute(self):
        print("### MLP - REDE PERCEPTRON MULTICAMADAS ###")
        print("PARÂMETROS: ")
        print("\t Taxa de aprendizagem: ", self.epochs)
        print("\t Número máximo de épocas: ", self.learning_rate)
        print("\t Total de realizações: ", self.realizations, "\n")
        rates = []
        q = 0
        error = []
        sqrt = []

        for i in range(self.realizations):
            print("\n### REALIZAÇÃO ", (i+1), "###")
            np.random.shuffle(self.samples)  # shuffle entre realizações
            self.samples = self.pr.insertbias(self.samples)
            training_set, test_set = train_test_split(self.samples, test_size=self.k_size)
            # q = self.crossvalidation(training_set)
            print("\nQUANTIDADE DE NEURONIOS: ", 12)
            mlp = MLP.multilayer_perceptron(self.epochs, self.learning_rate)
            mlp.initData(12, self.n_class)
            mlp.initSamples(training_set, test_set, self.n_class)
            mlp.initWeigths()
            print("### FASE DE TREINAMENTO ###")
            mlp.training(self.n_class, self.option)
            print("### FASE DE TESTE ###")
            rate = mlp.test(self.n_class, self.option)
            rates.append(rate)
            if(self.option <= 2):
                print("### TAXA: ", rate)
            elif (self.option == 3):
                self.training_errors.append(mlp.training_errors)
                self.test_errors.append(mlp.test_errors)
                self.allw.append(mlp.allw)
                mse = np.mean(mlp.test_errors)
                rmse = np.sqrt(mse)
                error.append(mse)
                sqrt.append(rmse)

                print("MSE: & ", round(mse, 5))
                print("RMSE: & ", round(rmse, 5))
                print("##########################")

            del mlp
        if(self.option <= 2):
            rates = np.array(rates)
            print("Acuracia: ", rates.mean())
            print("Desvio Padrão: ", rates.std())
        else:
            print("MSE médio: ", round((sum(error) / self.realizations), 5))
            print("RMSE médio: ", round((sum(sqrt) / self.realizations), 5))
            self.postRegression()

        print("### FIM DO MLP ###")

    def postRegression(self):
        auxmin = 1000
        min = 0

        self.test_errors = np.array(self.test_errors)
        self.training_errors = np.array(self.training_errors)
        self.allw = np.array(self.allw)
        j=0
        for i in self.test_errors:
            if i[j] <= auxmin:
                min = j
                auxmin = i[j]
            j+=1

        print('Desvio Padrão: ', np.std(self.test_errors[0]))
        print('Plotando dados com o MSE minimo: ')

        ut.plotError(self.training_errors[0][min])
        ut.plotData2d(self.samples, self.allw[0][min])
