import Parameterize as pr
import GridSearch as gs

PATH1 = "C:/Users/Mariano/Documents/Projetos/rna/MLP/data/"
PATH2 = "~/PycharmProjects/rna/MLP/data/"
# Base 0 - Iris; 
# Base 1 - Coluna Vertebral
# Base 2 - Dermatologia
# Base 3 - Câncer de Mama
# Base 4 - XOR
# Base 5 - Artificial
TYPE = 0

# Função 0 - Degrau
# Função 1 - Logistica
# Função 2 - Tangente Hiperbolica
# Função 3 - Linear
TYPE2 = 1
REALIZATIONS = 1
RATE = 0.05
EPOCHS = 500

def main():

    parameter = pr.Parameterize(PATH2, TYPE, TYPE2)
    n_neurons_output = parameter.getNeurons()
    # ind = parameter.getSamples().shape[1]
    GS = gs.GridSearch(parameter, REALIZATIONS, EPOCHS, RATE, TYPE2, n_neurons_output, 5, 4, 16)
    GS.execute()

if __name__ == "__main__":
    main()
