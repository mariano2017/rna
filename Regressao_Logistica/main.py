from Regressao_Logistica.Read import Read
from Regressao_Logistica.logistic_regression import Regressao_Logistica

# Caminho da pasta de databases
PATH = "./data/iris.data"

# Escolher Database:
# 0 - Iris 4 atributos
# 1 - Iris 2 atributos (X1 e X2)
# 2 - Iris 2 atributos (X3 e X4)
TYPE = 1

# Nome da classe principal
NAMECLASS = "Iris-setosa"

# Nº de realizações
REALIZATIONS = 20

# Taxa de Aprendizagem
RATE = 1e-4

# Nº de épocas
EPOCHS = 1000

# Nome no Plot
NAMETYPE = "IRIS - X1 2 X2" if TYPE == 1 else "IRIS - X3 2 X4"


def main():
    data = Read(PATH, TYPE, NAMECLASS)
    regression = Regressao_Logistica(data.get_samples(), RATE, EPOCHS, TYPE, NAMETYPE)
    regression.execute(REALIZATIONS)

if __name__ == "__main__":
    main()
