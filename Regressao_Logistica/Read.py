import pandas as pd
import numpy as np
import math


class Read:

    def __init__(self, path, type, nameclass):
        self.path = path
        self.samples = []
        self.samples = self.pattern(type, nameclass=nameclass)
        self.parameterize()
        np.random.shuffle(self.samples)

    def read_data(self):
        return np.array(pd.read_csv(self.path, delimiter=",", header=None))

    def parameterize(self):
        self.rows = len(self.samples)
        self.n_atributos = len(self.samples[0])
        self.training_size = math.floor(self.rows * 0.8)
        self.test_size = self.rows - self.training_size

    def normalize(self, dataset):
        for i in range(dataset.shape[1]-1):
            max_ = max(dataset[:, i])
            min_ = min(dataset[:, i])
            for j in range(dataset.shape[0]):
                dataset[j, i] = (dataset[j, i] - min_) / (max_ - min_)
        return dataset

    def pattern(self, pattern, nameclass):
        if(pattern == 0):
            dataset = self.read_data()
            dataset = self.get_class(nameclass, dataset)
            dataset = self.normalize(dataset)
        elif(pattern == 1):
            dataset = self.read_data()
            dataset = dataset[:, [0, 1, -1]]
            dataset = self.get_class(nameclass, dataset)
            dataset = self.normalize(dataset)
        elif(pattern == 2):
            dataset = self.read_data()
            dataset = dataset[:, [2, 3, -1]]
            dataset = self.get_class(nameclass, dataset)
            dataset = self.normalize(dataset)

        return np.array(dataset, dtype=np.float64)

    def get_class(self, group, dataset):
        size = len(dataset[0]) - 1
        for i in range(len(dataset)):
            dataset[i][size] = 1 if dataset[i][size] == group else -1
        return dataset

    def get_samples(self):
        return self.samples
