import numpy as np
import math
from scipy.special import expit
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt


class Regressao_Logistica:

    def __init__(self, data, rate, epochs, type, name_type=""):
        self.learning_rate = rate
        self.epochs = epochs
        self.weights = []
        self.type = name_type
        self.plotdata = data
        self.printPlot = True if(type != 0) else False
        self.samples = self.insert_bias(data)
        self.training_set = []
        self.test_set = []
        self.percentage = 80 / 100
        self.rows = len(data)
        self.n_attributes = len(data[0])
        self.allerrors = []
        self.training_size = math.floor(self.rows * self.percentage)
        self.test_size = self.rows - self.training_size

    def insert_bias(self, data):
        group = []
        for i in range(len(data)):
            group.append(np.insert(data[i], 0, -1))
        group = np.asarray(group)
        return group

    def divide_samples(self, dataset):
        self.training_set, self.test_set = train_test_split(dataset, test_size=0.20)

    def split_samples(self, dataset):
        return np.array(dataset[:, 0:self.n_attributes]), np.array(dataset[:, -1])

    def gradient_descendent(self, x, y, u):
        n = (y * x)
        d = 1 + np.exp(u)
        return (np.array(sum(n/d), ndmin=2)).T

    def error(self, u):
        u = (-1) * u
        return np.log(1 - np.exp(u))

    def sigmoid_logistic(self, y):
        return expit(-y)

    def predict_point(self, dataset):
        x = np.array(dataset[0:len(dataset)-1])
        u = x.dot(self.weights)
        return 1 / 1 + math.exp(u)

    def predict(self, y, m):
        y_pred = np.zeros((1, m))
        for i in range(y.shape[0]):
            y_pred[0][i] = 1 if y[i][0] > 0.5 else -1
        return y_pred

    def training(self, training_set):
        epochs = 0
        x_train, y_train = self.split_samples(training_set)
        y_train = np.array(y_train, ndmin=2).T

        for i in range(self.epochs):
            u = y_train * x_train.dot(self.weights)
            h = self.sigmoid_logistic(u)
            y = self.predict(h, x_train.shape[0])
            error = self.error(h)
            self.allerrors.append(error)
            self.weights = self.weights + self.gradient_descendent(x_train, y_train, u) * self.learning_rate
            epochs = i

        return self.weights, epochs, accuracy_score(y.T, y_train)

    def test(self, test_set):
        x_test, y_test = self.split_samples(test_set)
        y_test = np.array(y_test, ndmin=2).T
        u = y_test * x_test.dot(self.weights)
        h = self.sigmoid_logistic(u)
        y = self.predict(h, x_test.shape[0])
        y_test = np.array(y_test, ndmin=2)
        return accuracy_score(y.T, y_test)

    def execute(self, realizations):
        print("### REGRESSÃO LOGÍSTICA ###")
        print("PARÂMETROS: ")
        print("\t Taxa de aprendizagem: ", self.learning_rate)
        print("\t Número máximo de épocas: ", self.epochs)
        print("\t Total de realizações: ", realizations, "\n")
        rates = []

        for i in range(realizations):
            self.reset()
            np.random.shuffle(self.samples)
            self.divide_samples(self.samples)
            print("### REALIZAÇÃO ", (i+1), "###")
            print("### FASE DE training ###")
            self.weights, self.epochs, accuracy = self.training(self.training_set)
            print("Taxa de treinamento: ", accuracy)
            print("Quantidade de épocas: ", self.epochs)
            print("Vetor W final: ", self.weights)
            print("### FASE DE TESTES ###")
            taxa = self.test(self.test_set)
            print("Taxa de acerto: ", taxa, "\n")
            rates.append(taxa)

        rates = np.array(rates)
        self.allerrors = np.array(self.allerrors)
        print("Acurácia: ", rates.mean())
        print("Variância da Acurácia: ", rates.var())
        print("Variância do Erro: ", self.allerrors.var())
        print("Desvio Padrão da Acurácia: ", rates.std())

        if(self.printPlot):
            self.plot(self.training_set)
            self.plot(self.test_set)
        print("### FIM REGRESSÃO LOGÍSTICA ###")

    def reset(self):
        self.weights = np.random.rand(self.n_attributes, 1)
        self.bias = 0

    def plot(self, dataset):
        dataset1 = []
        dataset2 = []
        m = len(dataset)
        aux = 0

        #Separando as classes
        for i in range(len(dataset)):
            if(dataset[i][self.n_attributes] == 1):
                dataset1.append(dataset[i])
            else:
                dataset2.append(dataset[i])

        #Setando a superficie
        # for i in np.arange(-0.1, 1.1, 0.01):
        #     for j in np.arange(-0.1, 1.1, 0.01):
        #         if (self.predict_point(np.asarray([-1, i, j, 1])) > 0.5):
        #             plt.scatter(i, j, marker=".", c="#00FF7F")
        #         else:
        #             plt.scatter(i, j, marker=".", c="#F08080")

        #Setando Classe 1
        for i in dataset1:
            plt.scatter(i[1], i[2], marker="^", c="#008000")

        #Setando Classe 0
        for k in dataset2:
            plt.scatter(k[1], k[2], marker="*", c="#B22222")

        #Printando Gráfico
        plt.grid(True)
        plt.xlabel("X1")
        plt.ylabel("X2")
        plt.title("Superfície de decisão - " + str(self.type))
        plt.show()

