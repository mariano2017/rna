import Parameterize
import Perceptron

# Caminho da pasta de databases
PATH = "./data/iris.data"

# Escolher Database:
# 0 - Iris 4 atributos
# 1 - Iris 2 atributos (X1 e X2)
# 2 - Iris 2 atributos (X3 e X4)
# 3 - Porta AND PLOT
# 4 - Porta OR PLOT
TYPE = 0

# Nº de realizações
REALIZATIONS = 20

# Taxa de Aprendizagem
RATE = 0.05

# Nº de épocas
EPOCHS = 300

if TYPE == 1:
    NAMETYPE = "IRIS - X1 2 X2"
elif TYPE == 2:
    NAMETYPE = "IRIS - X3 2 X4"
elif TYPE == 3:
    NAMETYPE = "Artificial AND"
else:
    NAMETYPE = "Artificial OR"

def main():

    parameter = Parameterize.Parameterize(PATH, TYPE)
    Perceptron.Perceptron(parameter.get_samples(), REALIZATIONS, RATE, EPOCHS, TYPE, NAMETYPE)

if __name__ == "__main__":
    main()
