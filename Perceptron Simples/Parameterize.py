import pandas as pd
import numpy as np
import random
import math

class Parameterize():

    def __init__(self, path, type):
        self.path = path
        self.samples = []
        self.samples = self.pattern(type)
        self.parameterize()
        np.random.shuffle(self.samples)

    def read_data(self):
        return np.array(pd.read_csv(self.path, delimiter=",", header=None))

    def parameterize(self):
        self.rows = len(self.samples)
        self.n_atributos = len(self.samples[0])
        self.training_size = math.floor(self.rows * 0.8)
        self.test_size = self.rows - self.training_size

    def normalize(self, dataset):
        for i in range(dataset.shape[1]-1):
            max_ = max(dataset[:, i])
            min_ = min(dataset[:, i])
            for j in range(dataset.shape[0]):
                dataset[j, i] = (dataset[j, i] - min_) / (max_ - min_)
        return dataset

    def pattern(self, pattern):
        if(pattern == 0):
            dataset = self.read_data()
            dataset = self.get_class("Iris-setosa", dataset)
            dataset = self.normalize(dataset)
        elif(pattern == 1):
            dataset = self.read_data()
            dataset = dataset[:, [0, 1, -1]]
            dataset = self.get_class("Iris-setosa", dataset)
            dataset = self.normalize(dataset)
        elif(pattern == 2):
            dataset = self.read_data()
            dataset = dataset[:, [2, 3, -1]]
            dataset = self.get_class("Iris-setosa", dataset)
            dataset = self.normalize(dataset)
        elif(pattern == 3):
            dataset = self.artificial_and()
        else:
            dataset = self.artificial_or()

        return dataset

    def get_class(self, group, dataset):
        size = len(dataset[0]) - 1
        for i in range(len(dataset)):
            dataset[i][size] = 1 if dataset[i][size] == group else 0
        return dataset

    def artificial_and(self):
        artificial = []
        for i in range(400):
            if(i < 100):
                x = 0 + random.uniform(-0.1, 0.1)
                y = 0 + random.uniform(-0.1, 0.1)
                artificial.append([x, y, 0])
            elif(i  >= 100 and  i < 200):
                x = 1 + random.uniform(-0.1, 0.1)
                y = 0 + random.uniform(-0.1, 0.1)
                artificial.append([x, y, 0])
            elif (i >= 200 and i < 300):
                x = 0 + random.uniform(-0.1, 0.1)
                y = 1 + random.uniform(-0.1, 0.1)
                artificial.append([x, y, 0])
            else:
                x = 1 + (random.uniform(-0.1, 0.1))
                y = 1 + (random.uniform(-0.1, 0.1))
                artificial.append([x, y, 1])
        return np.array(artificial)


    def artificial_or(self):
        artificial = []
        for i in range(400):
            if(i < 100):
                x = 0 + random.uniform(-0.1, 0.1)
                y = 0 + random.uniform(-0.1, 0.1)
                artificial.append([x, y, 0])
            elif(i  >= 100 and  i < 200):
                x = 1 + random.uniform(-0.1, 0.1)
                y = 0 + random.uniform(-0.1, 0.1)
                artificial.append([x, y, 1])
            elif (i >= 200 and i < 300):
                x = 0 + random.uniform(-0.1, 0.1)
                y = 1 + random.uniform(-0.1, 0.1)
                artificial.append([x, y, 1])
            else:
                x = 1 + (random.uniform(-0.1, 0.1))
                y = 1 + (random.uniform(-0.1, 0.1))
                artificial.append([x, y, 1])
        return np.array(artificial)

    def get_samples(self):
        return self.samples
