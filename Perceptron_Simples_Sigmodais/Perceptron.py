import numpy as np
import math
import matplotlib.pyplot as plt
import Perceptron_Simples_Sigmodais.Functions as ft

class Perceptron:

    def __init__(self, data, rate, epochs, type, function, name_type=""):
        self.learning_rate = rate
        self.epochs = epochs
        self.weights = []
        self.type = name_type
        self.plotdata = data
        self.function = ft.Functions(function)
        self.printPlot = True if(type != 0) else False
        self.samples = data
        self.training_set = []
        self.test_set = []
        self.percentage = 80 / 100
        self.rows = len(data)
        self.n_attributes = len(data[0]) - 1
        self.training_size = math.floor(self.rows * self.percentage)
        self.test_size = self.rows - self.training_size

    def divide_samples(self):
        self.weights = self.reset(self.n_attributes)
        for i in range(self.rows):
            if(i < self.training_size):
                self.training_set.append(self.samples[i])
            else:
                self.test_set.append(self.samples[i])

    def activation(self, x):
        return np.dot(x[0:len(x)-1],self.weights[0])

    def update_weights(self, x, erro, y):
        self.weights[0] = self.weights[0] + (self.learning_rate) * (erro) * self.function.derivate(y) * x[0:len(x)-1]

    def iteration_error(self, x, y):
        d = self.desired_output(x)
        return d - y

    def predict(self, x):
        u = self.activation(x)
        return self.function.predict(u)

    def desired_output(self, x):
        return int(x[self.n_attributes])

    def training(self):
        epochs = 0
        while True:
            error = True
            np.random.shuffle(self.training_set)
            for i in range(self.training_size):
                y = self.predict(self.training_set[i])
                e = self.iteration_error(self.training_set[i], self.function.thresold(y))
                self.update_weights(self.training_set[i], e, y)
                if(e != 0):
                    error = False
            epochs += 1

            if error or epochs >= self.epochs:
                break
        return epochs

    def test(self):
        matrix = [[0, 0], [0, 0]]
        matrix = np.asarray(matrix)
        for i in range(self.test_size):
            y = self.function.thresold(self.predict(self.test_set[i]))
            d = self.desired_output(self.test_set[i])
            j = 0 if y == -1 else y
            k = 0 if d == -1 else d
            matrix[j][k] += 1
        rate = self.rate_calculation(matrix)
        return rate

    def rate_calculation(self, matrix):
        hits = np.trace(matrix)
        print("Matriz de confusão: \n", matrix)
        return hits / self.test_size

    def execute(self, realizations):
        print("### REDE PERCEPTRON ###")
        print("PARÂMETROS: ")
        print("\t Taxa de aprendizagem: ", self.learning_rate)
        print("\t Número máximo de épocas: ", self.epochs)
        print("\t Total de realizações: ", realizations, "\n")
        rates = []

        self.divide_samples()
        for i in range(realizations):
            print("### REALIZAÇÃO ", (i+1), "###")
            np.random.shuffle(self.samples)  # shuffle entre realizações
            print("### FASE DE training ###")
            self.weights = self.reset(self.n_attributes)
            epoch = self.training()
            print("Vetor W final: ", self.weights)
            print("Número de épocas: ", epoch)
            print("### FASE DE TESTES ###")
            taxa = self.test()
            print("Taxa de acerto: ", taxa, "\n")
            rates.append(taxa)

        rates = np.array(rates)
        print("Acurácia: ", rates.mean())
        print("Variância: ", rates.var())
        print("Desvio Padrão: ", rates.std())
        self.plot_acuracy(rates)

        if(self.printPlot):
            self.plot(self.training_set)
            self.plot(self.test_set)
        print("### FIM PERCEPTRON ###")

    def reset(self, n):
        return np.random.rand(1, n)

    def plot_acuracy(self, rates):
        plt.plot(range(1, len(rates) + 1), rates)
        plt.xlabel('Realizações')
        plt.ylabel('Taxas')
        plt.show()

    def plot(self, dataset):
        dataset1 = []
        dataset2 = []
        aux = 0

        #Separando as classes
        for i in range(len(dataset)):
            if(dataset[i][self.n_attributes] == 1):
                dataset1.append(dataset[i])
            else:
                dataset2.append(dataset[i])

        #Setando a superficie
        for i in np.arange(-0.1, 1.1, 0.01):
            for j in np.arange(-0.1, 1.1, 0.01):
                if (self.function.thresold(self.predict([-1, i, j, 1])) > 0):
                    plt.scatter(i, j, marker=".", c="#00FF7F")
                else:
                    plt.scatter(i, j, marker=".", c="#F08080")

        #Setando Classe 1
        for i in dataset1:
            plt.scatter(i[1], i[2], marker="^", c="#008000")

        #Setando Classe 0
        for k in dataset2:
            plt.scatter(k[1], k[2], marker="*", c="#B22222")

        #Printando Gráfico
        plt.grid(True)
        plt.xlabel("X1")
        plt.ylabel("X2")
        plt.title("Superfície de decisão - " + str(self.type))
        plt.show()

