import numpy as np
import math


class Functions:

    def __init__(self, option):
        self.option = option
        self.p = []
        self.d = []
        self.t = []

    def predict(self, u):
        if self.option == 0:
            return 1 if u >= 0 else 0
        elif self.option == 1:
            return 1 / (1 + math.exp(-u))
        elif self.option == 2:
            return (1 - math.exp(-u))/(1 + math.exp(-u))

    def derivate(self, y):
        if self.option == 0:
            return 1
        elif self.option == 1:
            return y * (1 - y)
        elif self.option == 2:
            return 0.5 * (1 - (y*y))

    def thresold(self, y):
        if self.option == 0:
            return y
        elif self.option == 1:
            return 1 if y >= 0.5 else 0
        elif self.option == 2:
            return 1 if y >= 0 else -1
