import numpy as np
import cvxopt
from sklearn.metrics import confusion_matrix

class SVM:

    def __init__(self,dataset):
        self.training_set, self.test_set = dataset

    def fit(self, n, X, y):
        P = cvxopt.matrix([[1,0,0], [0,1,0], [0,0,0]], tc='d')
        q = cvxopt.matrix(np.zeros(3))
        aux = np.concatenate((X, np.ones((n,1))), axis=1)
        y = np.array(y, ndmin=2)
        G = cvxopt.matrix((y.T * aux) * -1)
        h = cvxopt.matrix(np.ones(n) * -1)
        solution = cvxopt.solvers.qp(P, q, G, h)
        return np.ravel(solution['x'])

    def predict(self, X):
        return np.sign(X)

    def rate_calculation(self, matrix):
        hits = np.trace(matrix)
        return (hits / len(self.test_set))

    def execute(self):
        print("### SVM ###")
        rates = []
        print("### FASE DE training ###")
        X = self.fit(self.training_set.shape[0], self.training_set[:, 0:self.training_set.shape[1] -1], np.array(self.training_set[:, -1], ndmin=2))
        self.weights = X[:2]
        self.b = X[2]
        print("Vetor W final: ", self.weights)
        print("### FASE DE TESTES ###")
        u = np.dot(self.test_set[:, 0:self.test_set.shape[1] -1], np.array(self.weights, ndmin=2).T) + self.b
        y_pred = self.predict(u)
        matriz = confusion_matrix(self.test_set[:, -1], y_pred)
        taxa = self.rate_calculation(matriz)
        print("Taxa de acerto: ", taxa, "\n")
        rates.append(taxa)
        rates = np.array(rates)
        print("Acurácia: ", rates.mean())
        print("### SVM ###")

        return rates.mean()

