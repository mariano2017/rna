import random
import numpy as np
import matplotlib.pyplot as plt

class Read():

    def __init__(self, qtd_train, qtd_test):
        self.qtd_train = qtd_train
        self.qtd_test = qtd_test
        self.samples_train, self.samples_test = self.cal_class()
        self.polynomial

    def target(self):
        x = [random.uniform(-1.0, 1.0), random.uniform(-1.0, 1.0)]
        y = [random.uniform(-1.0, 1.0), random.uniform(-1.0, 1.0)]
        coefficients = np.polyfit(x, y, 1)
        self.polynomial = np.poly1d(coefficients)
        return x, y

    def getclass(self, x1, x2, d1, d2):
        a = np.array([x1[0], x2[0]])
        b = np.array([x1[1], x2[1]])
        p = np.array([d1, d2])
        a = np.cross(p - a, b - a)
        if a >= 0:
            return 1
        else:
            return -1

    def cal_class(self):
        dataset = self.artificial()
        dataset_train, dataset_test = dataset
        x_axis, y_axis = self.target()

        for i in dataset_train:
            i[-1] = self.getclass(x_axis, y_axis, i[0], i[1])

        for j in dataset_test:
            j[-1] = self.getclass(x_axis, y_axis, j[0], j[1])

        y_train = dataset_train[:, -1]
        y_test = dataset_test[:, -1]

        if len(np.unique(y_train)) == 1:
            if len(np.unique(y_test)) == 1:
                return self.cal_class()

        return dataset_train, dataset_test

    def artificial(self):
        artificial_train = []
        artificial_test = []
        for i in range(self.qtd_train):
            x = random.uniform(-1.0, 1.0)
            y = random.uniform(-1.0, 1.0)
            artificial_train.append([x, y, 0])

        for i in range(self.qtd_test):
            x = random.uniform(-1.0, 1.0)
            y = random.uniform(-1.0, 1.0)
            artificial_test.append([x, y, 0])
        return np.array(artificial_train), np.array(artificial_test)

    def plot(self, dataset, x_axis, y_axis):
        plt.plot(x_axis, y_axis)
        plt.plot(dataset[:, 0], dataset[:, 1], 'go')
        plt.grid('on')
        plt.show()

    def get_samples(self):
        return self.samples_train, self.samples_test
