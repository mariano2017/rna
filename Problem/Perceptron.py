import numpy as np
import math

class Perceptron:

    def __init__(self, dataset, c, rate, epochs):
        self.c = c
        self.learning_rate = rate
        self.epochs = epochs
        self.weights = []
        self.training_set , self.test_set = dataset
        self.training_set = self.insert_bias(self.training_set)
        self.test_set = self.insert_bias(self.test_set)

    def insert_bias(self, data):
        group = []
        for i in range(len(data)):
            group.append(np.insert(data[i], 0, -1))
        group = np.asarray(group)
        return group

    def activation(self, x):
        return np.dot(x[0:len(x)-1],self.weights)

    def update_weights(self, x, erro):
        self.weights = self.weights + (self.learning_rate) * (erro) * x[0:len(x)-1]

    def predict(self, u):
        return np.where(u >= 0.0, 1, -1)

    def training(self):
        epochs = 0
        hits = []
        while epochs < self.epochs:
            hit = 0
            for i in range(len(self.training_set)):
                u = self.activation(self.training_set[i])
                y = self.predict(u)
                e = self.training_set[i][-1] - y
                self.update_weights(self.training_set[i], e)
                if e == 0 and epochs == self.epochs - 1:
                    hit += 1.0
            hits.append(hit/len(self.training_set))
            epochs += 1
            np.random.shuffle(self.training_set)
        hits = np.array(hits)
        return self.weights, (hits[-1])

    def test(self):
        hits = 0
        for i in range(len(self.test_set)):
            u = self.activation(self.test_set[i])
            y = self.predict(u)
            e = self.test_set[i][-1] - y
            if e != 0:
               hits  += 1

        return 1 - (hits/len(self.test_set))

    def execute(self):
        print("### REDE PERCEPTRON ###")
        print("PARÂMETROS: ")
        print("\t Taxa de aprendizagem: ", self.learning_rate)
        print("\t Número máximo de épocas: ", self.epochs)
        rates = []
        # self.training_set, self.test_set = train_test_split(self.samples, test_size=0.20)
        print("### FASE DE training ###")
        self.weights = self.reset(len(self.training_set[0])-1)
        self.weights, rate_train = self.training()
        print("Vetor W final: ", self.weights)
        print("Taxa de acerto do treino: ", rate_train)
        print("### FASE DE TESTES ###")
        taxa = self.test()
        print("Taxa de acerto: ", taxa, "\n")
        rates.append(taxa)

        rates = np.array(rates)
        print("Acurácia: ", rates.mean())
        print("### FIM PERCEPTRON ###")
        return rates.mean()

    def reset(self, n):
        return np.zeros(n)