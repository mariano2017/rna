from Problem.Read import Read as read
from Problem.Perceptron import Perceptron as pc
from Problem.SVM import SVM as svm

#QUANTIDADE DE AMOSTRAS
SAMPLES_TRAIN = 10

SAMPLES_TEST = 1000

# Nº de execuções
EXECUTIONS = 100

# Taxa de Aprendizagem
RATE = 0.1

# Nº de épocas
EPOCHS = 100

def main():
    hits_svm = 0.
    hits_pc = 0.
    for i in range(EXECUTIONS):
        data = read(SAMPLES_TRAIN, SAMPLES_TEST)
        p = pc(data.get_samples(), data.polynomial, RATE, EPOCHS)
        rate_pc = p.execute()
        s = svm(data.get_samples())
        rate_svm = s.execute()
        print(rate_pc, rate_svm)
        if rate_pc > rate_svm:
            hits_pc += 1.
        else:
            hits_svm += 1.

    print("PERCEPTRON: ", hits_pc/EXECUTIONS)
    print("SVM: ", hits_svm/EXECUTIONS)

if __name__ == "__main__":
    main()
