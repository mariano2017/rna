import numpy as np
import random
from sklearn.metrics import mean_squared_error


class ELM:

    def __init__(self):
        self.training_set = []
        self.test_set = []
        self.n_layers = 0
        self.hiddenLayer = 0  # -> quantidade neurônios camada oculta
        self.outLayer = 0  # -> quantidade neurônios camada de saida
        self.n_attributtes = 0  # -> quantidade de variávies/atributos
        self.rmse = 0
        self.beta = []
        self.y = []
        self.test_errors = []
        self.initWeigths()

    def initData(self, n_hiddenLayer=1, n_outLayer=1, n_layers=1):
        self.n_layers = n_layers
        self.hiddenLayer = n_hiddenLayer  # -> quantidade neurônios camada oculta
        self.outLayer = n_outLayer  # -> quantidade neurônios camada de saida

    def initSamples(self, training_set, test_set, outputs):
        self.training_set = training_set
        self.test_set = test_set
        self.n_attributtes = training_set.shape[1]-outputs  # -> quantidade de variávies/atributos

    def initWeigths(self):
        # -> inicializa os pesos de forma randomica
        self.weights_hidden = np.random.randn(self.hiddenLayer, self.n_attributtes)
        self.weights_output = []

    def predict(self, x, option=1):
        x = np.array(x, dtype=np.float64)
        if(option == 0):
            return self.step(x, option)
        elif (option == 1):
            return 1.0 / (1.0 + np.exp(-x))
        elif (option == 2):
            return (1 - np.exp(-x))/(1 + np.exp(-x))
        else:
            return x

    def derivate(self, y, option=2):
        if (option == 1):
            return y * (1.0 - y)
        if (option == 2):
            return (1/2)*(1 - y*y)
        else:
            return 1

    def step(self, outputs, option):
        u = np.array(outputs, dtype=np.float64)
        aux = np.zeros(np.array(u).shape)
        maximum = np.argmax(u)
        aux[maximum] = 1
        return aux

    def activation(self, inputs, weigths, option=1):
        u = np.dot(inputs, weigths)
        y = []
        for i in range(u.shape[0]):
            aux = []
            for j in range(u.shape[1]):
                aux.append(self.predict(j, option=option))
            y.append(aux)

        return np.array(y)

    def getWeigths(self):
        return self.weights_hidden, self.weights_output

    def test(self, inputs, outputs, option=2):
        x = inputs[:, 0:self.n_attributtes]
        x = np.array(x)

        u = np.dot(self.weights_hidden, x.T)
        h = self.predict(u, option)
        out = np.dot(h.T, self.beta)

        # h = np.c_[-np.ones(h.shape[0]), h]
        Y = self.step(h.dot(out), option)

        if(option <= 2):
            hits = 0
            for i in range(inputs.shape[0]):
                d = inputs[:, -outputs:inputs.shape[1]]
                if np.array_equal(out[i], d[i]):
                    hits += 1

            return hits/inputs.shape[0]
        else:
            d = inputs[:, -outputs:inputs.shape[1]]
            return self.estimate(d, Y)

    def training(self, inputs, n_outputs, option=1):
        x = inputs[:, 0:self.n_attributtes]
        d = inputs[:, -n_outputs:inputs.shape[1]]

        u = np.dot(self.weights_hidden, x.T)
        h = self.predict(u, option)
        h_ = np.linalg.pinv(h)
        self.beta = np.dot(h_.transpose(), d)
        self.y = np.dot(h.transpose(), self.beta)

        # x = np.array(x)
        #
        # g = x.dot(self.weights_hidden)
        # h = self.predict(g, option)
        # h = np.c_[-np.ones(h.shape[0]), h]
        # ht = np.transpose(h)
        # self.weights_output = np.linalg.inv(np.dot(h, ht)).dot(d)
        # self.weights_output = np.dot(_H, np.dot(, d))
