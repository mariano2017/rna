from sklearn.model_selection import KFold
import numpy as np
import ELM
import Parameterize as pr

class GridSearch():

    def __init__(self, data, realizations, rate, option, n_class, k, nrangemin=0, nrangemax=10):

        self.learning_rate = rate
        self.k = k
        self.pr = data
        self.samples = data.getSamples()
        self.training_size = 0.8
        self.n_attributtes = self.samples.shape[1] - n_class
        self.option = option
        self.nrangemin = nrangemin
        self.nrangemax = nrangemax
        self.n_class = n_class
        self.realizations = realizations

    def divide(self, data):
        np.random.shuffle(data)
        training_set = data[0: int(len(data) * self.training_size)]
        test_set = data[int(len(data) * self.training_size):]
        return training_set, test_set

    def crossvalidation(self, dataset):
        kf = KFold(n_splits=self.k)

        acc = []
        for i in range(self.nrangemin, self.nrangemax):

            hitratings = []
            for train_index, test_index in kf.split(dataset):
                elm = ELM.ELM()
                train, test = dataset[train_index], dataset[test_index]
                elm.initData(i, self.n_class)
                elm.initSamples(train, test, self.n_class)
                elm.initWeigths()
                elm.training(train, self.n_class, self.option)
                hitratings.append(np.array(elm.test(test, self.n_class, self.option)))
                del elm

            acc.append((np.array(hitratings).mean()))

        amax = max(acc)
        print("\nVETOR DE ACURACIAS: ", acc)
        print("\n\nAMAX: ", amax)
        print("\nINICIAL: ", self.nrangemin)
        print("\nQUANTIDADE DE NEURONIOS: ",self.nrangemin + acc.index(amax) )

        return self.nrangemin + acc.index(amax)
    
    def execute(self):
        print("### ELM ###")
        print("PARÂMETROS: ")
        # print("\t Taxa de aprendizagem: ", self.learning_rate)
        print("\t Total de realizações: ", self.realizations, "\n")
        rates = []
        q = 0
        error = []
        sqrt = []
        print(self.option)

        for i in range(self.realizations):
            print("### REALIZAÇÃO ", (i+1), "###")
            np.random.shuffle(self.samples)  # shuffle entre realizações
            self.samples = self.pr.normalize(self.samples[:, 1:], self.n_class)
            self.samples = self.pr.insertbias(self.samples)
            training_set, test_set = self.divide(self.samples)
            q = self.crossvalidation(training_set)
            elm = ELM.ELM()
            elm.initData(q, self.n_class)
            elm.initSamples(training_set, test_set, self.n_class)
            elm.initWeigths()
            print("### FASE DE TREINAMENTO ###")
            elm.training(training_set, self.n_class, self.option)
            print("### FASE DE TESTE ###")
            rate = elm.test(test_set, self.n_class, self.option)
            if (self.option <= 2):
                print("### TAXA: ", rate)
                rates.append(rate)

            if (self.option == 3):
                # self.training_errors.append(mlp.test_errors)
                # self.test_errors.append(mlp.test_errors)
                # self.allw.append(mlp.allw)
                # self.allm.append(mlp.allm)
                mse = np.mean(rate)
                rmse = np.sqrt(mse)
                error.append(mse)
                sqrt.append(rmse)

                print("MSE: & ", round(mse, 5))
                print("RMSE: & ", round(rmse, 5))
                print("##########################")
            del elm
        if (self.option <= 2):
            rates = np.array(rates)
            print("Acuracia: ", rates.mean())
            print("Desvio Padrão: ", rates.std())
        else:
            print("MSE médio: ", round((sum(error) / self.realizations), 5))
            print("RMSE médio: ", round((sum(sqrt) / self.realizations), 5))
        print("### FIM DO ELM ###")

