import pandas as pd
import numpy as np
import random
import math

class Parameterize():

    def __init__(self, path, type1, type2):
        self.path = path + self.dataname(type1) 
        self.samples = []
        self.tamN = 0
        self.samples, self.tamN = self.dataselect(type1, type2)

    def read(self):
        return np.array(pd.read_csv(self.path, delimiter=",", header=None))

    def dataname(self, opt):
        if opt == 0:
            return "iris.data"
        elif opt == 1:
            return "column_3C.dat"
        elif opt == 2:
            return "dermatology.data"
        elif opt == 3:
            return "breastcancer.data"
        else:
            return ""

    def dataselect(self, opt, opt2):
        if opt == 0:
            dataset = self.read()
            return self.classSelection(opt2, dataset)
        elif opt == 1:
            dataset = self.read()
            return self.classSelection(opt2, dataset)
        elif opt == 2:
            dataset = self.read()
            return self.classSelection(opt2, dataset)
        elif opt == 3:
            dataset = self.read()
            dataset = dataset[:, 1:]
            return self.classSelection(opt2, dataset)
        elif opt == 4:
            return self.problemXOR(opt2)
        elif opt == 5:
            return self.artificial(opt2)

    def classSelection(self, opt, dataorigin):
        dataset = dataorigin
        x = dataset[:, 0:len(dataset) - 1]
        x = np.delete(x, -1, axis = 1)
        y = dataset[:, -1]

        if opt == 0 or opt == 1:
            # labels = [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
            aux = 0
        else:
            # labels = [[1, -1, -1], [-1, 1, -1], [-1, -1, 1]]
            aux = -1

        datasetclasses = np.unique(dataset[:, -1])
        
        for i in range(len(datasetclasses)):
            data = []
            for j in range(len(y)):
                if y[j] == datasetclasses[i]:
                    data.append(1)
                else:
                    data.append(aux)
            x = np.c_[x, data]

        x = self.insertbias(x)
        return x, len(datasetclasses)

    def insertbias(self, dataset):
        new = []
        for i in range(len(dataset)):
            new.append(np.insert(dataset[i], 0, -1))
        return np.asarray(new)

    def normalize(self, dataset, attrNumber):
        for i in range(dataset.shape[1]-attrNumber):
            max_ = max(dataset[:, i])
            min_ = min(dataset[:, i])
            for j in range(dataset.shape[0]):
                dataset[j, i] = (float(dataset[j, i]) - float(min_)) / float(max_ - min_)
        return dataset

    def problemXOR(self, opt):
        artificial = []
        label = []
        if opt == 2:
            aux = -1
        else:
            aux = 0

        for i in range(200):
            if i < 50:
                x = 0.0 + np.random.uniform(-0.1, 0.1)
                y = 0.0 + np.random.uniform(-0.1, 0.1)
                label = [1, aux]
            elif i < 100:
                x = 0.0 + np.random.uniform(-0.1, 0.1)
                y = 1.0 + np.random.uniform(-0.1, 0.1)
                label = [aux, 1]

            elif i < 150:
                x = 1.0 + np.random.uniform(-0.1, 0.1)
                y = 0.0 + np.random.uniform(-0.1, 0.1)
                label = [aux, 1]
            else:
                x = 1.0 + np.random.uniform(-0.1, 0.1)
                y = 1.0 + np.random.uniform(-0.1, 0.1)
                label = [1, aux]
            artificial.append([x, y, label[0], label[1]])
        
        artificial = self.insertbias(artificial)
        return np.array(artificial), len(label)

    def artificial(self, opt):
        dataset = []
        for i in range(500):
            y = 3 * math.sin(i) + 1
            dataset.append([i, y])

        return np.asarray(dataset), 1

    def getSamples(self):
        return self.samples

    def getNeurons(self):
        return self.tamN
