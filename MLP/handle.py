from MLP import multilayer_perceptron as mlp
import numpy as np
from sklearn.model_selection import train_test_split

class Handle:

    def __init__(self, data, realizations, epochs, rate, n_class, n_neurons):
        self.learning_rate = rate
        self.epochs = epochs
        self.n_neurons = n_neurons
        self.samples = data
        self.n_attributtes = self.samples.shape[1] - n_class
        self.n_class = n_class
        self.realizations = realizations

    def insertbias(self, dataset):
        new = []
        for i in range(len(dataset)):
            new.append(np.insert(dataset[i], 0, -1))
        return np.asarray(new)
    
    def execute(self):
        print("### MLP - REDE PERCEPTRON MULTICAMADAS ###")
        print("PARÂMETROS: ")
        print("\t Taxa de aprendizagem: ", self.learning_rate)
        print("\t Número máximo de épocas: ", self.epochs)
        print("\t Total de realizações: ", self.realizations, "\n")
        self.samples = self.insertbias(self.samples)
        training_set, test_set = train_test_split(self.samples, test_size=0.20)
        rates = []

        for i in range(self.realizations):
            print("\n### REALIZAÇÃO ", (i+1), "###")
            print("\nQUANTIDADE DE NEURONIOS: ", self.n_neurons)

            #Inicializando os dados na class MLP
            alg = mlp.MLP(self.epochs, self.learning_rate)
            alg.init_quantity_layers(n_hiddenLayer=self.n_neurons, n_outLayer=self.n_class)
            alg.init_samples(training_set, test_set, self.n_class)
            alg.init_weigths()

            print("### FASE DE TREINAMENTO ###")
            alg.training(self.n_class)
            print("### FASE DE TESTE ###")
            rate = alg.test(self.n_class)
            rates.append(rate)
            print("### TAXA: ", rate)

            del alg

        rates = np.array(rates)
        print("Acuracia: ", rates.mean())
        print("Desvio Padrão: ", rates.std())
        print("### FIM DO MLP ###")