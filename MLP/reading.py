import pandas as pd
import numpy as np
import math

class Read:

    def __init__(self, path, type_dataset):
        self.path = path + self.dataname(type_dataset)
        self.samples = []
        self.tamN = 0
        self.samples, self.tamN = self.dataselect(type_dataset)

    def read(self, opt):
        if opt == 1:
            return np.array(pd.read_csv(self.path, delimiter=",", header=None), dtype=float)
        return np.array(pd.read_csv(self.path, delimiter=",", header=None))

    def dataname(self, opt):
        if opt == 0:
            return "iris.data"
        elif opt == 1:
            return "breastcancer.data"
        else:
            return ""

    def dataselect(self, opt):
        if opt == 0:
            dataset = self.read(opt)
            return self.classSelection(dataset, opt)
        elif opt == 1:
            dataset = self.read(opt)
            return self.classSelection(dataset, opt)
        elif opt == 2:
            return self.problemXOR()

    def classSelection(self, dataorigin, opt):
        if opt == 2:
            dataset = dataorigin[:, 1:len(dataorigin)]
        else:
            dataset = dataorigin

        x = dataset[:, 0:len(dataset) - 1]
        x = np.delete(x, -1, axis = 1)
        y = dataset[:, -1]

        datasetclasses = np.unique(dataset[:, -1])
        
        for i in range(len(datasetclasses)):
            data = []
            for j in range(len(y)):
                if y[j] == datasetclasses[i]:
                    data.append(1)
                else:
                    data.append(0)
            x = np.c_[x, data]

        x = self.normalize(x, len(datasetclasses))
        return x, len(datasetclasses)

    def normalize(self, dataset, attrNumber):
        for i in range(dataset.shape[1]-attrNumber):
            max_ = max(dataset[:, i])
            min_ = min(dataset[:, i])
            for j in range(dataset.shape[0]):
                dataset[j, i] = (float(dataset[j, i]) - float(min_)) / float(max_ - min_)
        return dataset

    def problemXOR(self):
        artificial = []
        label = []

        for i in range(200):
            if i < 50:
                x = 0.0 + np.random.uniform(-0.1, 0.1)
                y = 0.0 + np.random.uniform(-0.1, 0.1)
                label = [1, 0]
            elif i < 100:
                x = 0.0 + np.random.uniform(-0.1, 0.1)
                y = 1.0 + np.random.uniform(-0.1, 0.1)
                label = [0, 1]

            elif i < 150:
                x = 1.0 + np.random.uniform(-0.1, 0.1)
                y = 0.0 + np.random.uniform(-0.1, 0.1)
                label = [0, 1]
            else:
                x = 1.0 + np.random.uniform(-0.1, 0.1)
                y = 1.0 + np.random.uniform(-0.1, 0.1)
                label = [1, 0]
            artificial.append([x, y, label[0], label[1]])


        # artificial = self.insertbias(artificial)
        return np.array(artificial), len(label)

    def get_samples(self):
        return self.samples

    def get_n_neurons(self):
        return self.tamN
