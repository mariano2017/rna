import numpy as np
from scipy.special import expit

class MLP:

    def __init__(self, epochs, eta=0.1):
        self.learning_rate = eta
        self.epochs = epochs
        self.training_set = []
        self.test_set = []
        self.n_layers = 0 # -> quantidade de camadas na rede
        self.hiddenLayer = 0  # -> quantidade neurônios camada oculta
        self.outLayer = 0  # -> quantidade neurônios camada de saida
        self.n_attributtes = 0  # -> quantidade de variávies/atributos

    def init_quantity_layers(self, n_hiddenLayer=3, n_outLayer=2, n_layers=1):
        self.n_layers = n_layers # -> quantidade de camadas na rede
        self.hiddenLayer = n_hiddenLayer  # -> quantidade neurônios camada oculta
        self.outLayer = n_outLayer  # -> quantidade neurônios camada de saida

    def init_samples(self, training_set, test_set, outputs):
        self.training_set = training_set
        self.test_set = test_set
        self.n_attributtes = len(self.training_set[0]-outputs)  # -> quantidade de variávies/atributos

    def init_weigths(self):
        # -> inicializa os pesos de forma randomica
        self.weights_hidden = np.random.random((self.hiddenLayer, self.n_attributtes))
        self.weights_output = np.random.random((self.outLayer, self.hiddenLayer))

    def derivate(self, y):
        return y * (1 - y)

    def activation(self, u):
        u = np.array(u, dtype=np.float64)
        y = expit(u)
        return np.array(y)

    def get_weigths(self):
        return self.weights_hidden, self.weights_output

    def step(self, outputs):
        pos = np.amax(outputs)
        return np.where(outputs == pos, 1, 0)

    def feedforward(self, x):
        u = np.dot(x, self.weights_hidden.T)
        h = self.activation(u)
        v = np.dot(h, self.weights_output.T)
        y = self.activation(v)
        return y, h

    def backpropagation(self, error, h, y, x):
        y_delta = np.multiply(error, self.derivate(y))

        h_error = np.dot(y_delta, self.weights_output)
        h_delta = np.multiply(h_error, self.derivate(h))

        self.weights_output = self.adjust_weights(h, y_delta, self.weights_output)
        self.weights_hidden = self.adjust_weights(x, h_delta, self.weights_hidden)

    def adjust_weights(self, x, delta, w):
        error = np.ones((x.shape[0], delta.shape[0]), dtype=int) * np.asarray(delta)
        update = self.learning_rate * error.T * x
        return w + update

    def training(self, n_outputs):
        for i in range(self.epochs):
            for j in self.training_set:
                x, d = j[0:self.n_attributtes], j[-n_outputs:len(j)]
                y, h = self.feedforward(x)
                error = d - self.step(y)
                self.backpropagation(error, h, y, x)

            np.random.shuffle(self.training_set)


    def test(self, outputs):
        hits = 0
        for i in self.test_set:
            x, d = i[0:self.n_attributtes], i[-outputs:len(i)]
            u = np.dot(x, self.weights_hidden.T)
            h = self.activation(u)
            v = np.dot(h, self.weights_output.T)
            g = self.activation(v)
            y = self.step(g)

            if np.array_equal(y, d):
                hits += 1

        return hits/len(self.test_set)