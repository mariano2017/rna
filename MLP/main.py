from MLP import reading, handle

#Escolher o caminho da pasta dos arquivos
# PATH1 = "./data/"
PATH2 = "~/PycharmProjects/rna/MLP/data/"

# Base 0 - Iris;
# Base 1 - Câncer de Mama
# Base 2 - XOR
TYPE = 0

REALIZATIONS = 20
RATE = 0.01
EPOCHS = 500

def main():

    parameter = reading.Read(PATH2, TYPE)
    n_neurons_output = parameter.get_n_neurons()
    # print(parameter.get_samples()[0])
    GS = handle.Handle(parameter.get_samples(), REALIZATIONS, EPOCHS, RATE, n_neurons_output, 4)
    GS.execute()

if __name__ == "__main__":
    main()
