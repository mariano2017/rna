import pandas as pd
import numpy as np

class Parameterize():

    def __init__(self, path, datasetname , type, type2):
        self.path = path + datasetname 
        self.samples = []
        self.qtd_neurons = 0
        self.samples = self.dataselect(type, type2)
        self.samples = self.normalize(self.samples)
        self.samples = self.insertbias(self.samples)

    def read(self):
        return np.array(pd.read_csv(self.path, delimiter=",", header=None))

    def dataselect(self, opt, opt2):
        if opt == 0:
            dataset = self.read()
            return self.class_selection(opt2, dataset)
        elif opt == 1:
            dataset = self.read()
            dataset = dataset[:, [0, 1, -1]]
            return self.class_selection(opt2, dataset)
        elif opt == 2:
            dataset = self.read()
            dataset = dataset[:, [2, 3, -1]]
            return self.class_selection(opt2, dataset)
        elif opt == 3:
            dataset = self.read()
            dataset = dataset[:, [4, 5, -1]]
            return self.class_selection(opt2, dataset)
        else:
            return self.artificial(opt2)
    
    def class_selection(self, opt, dataorigin):
        dataset = dataorigin
        x = dataset[:, 0:len(dataset) - 1]
        x = np.delete(x, -1, axis = 1)
        y = dataset[:, -1]
        aux = 0 if opt == 0 or opt == 1 else -1

        datasetclasses = np.unique(dataset[:, -1])
        
        for i in range(len(datasetclasses)):
            data = []
            for j in range(len(y)):
                if y[j] == datasetclasses[i]:
                    data.append(1)
                else:
                    data.append(aux)
            x = np.c_[x, data]

        self.qtd_neurons = len(datasetclasses)
        return x


    def insertbias(self, dataset):
        new = []
        for i in range(len(dataset)):
            new.append(np.insert(dataset[i], 0, -1))
        return np.asarray(new)

    def normalize(self, dataset):
        for i in range(dataset.shape[1]-1):
            max_ = max(dataset[:, i])
            min_ = min(dataset[:, i])
            for j in range(dataset.shape[0]):
                dataset[j, i] = (dataset[j, i] - min_) / (max_ - min_)
        return dataset

    def artificial(self, opt):
        artificial = []
        aux = 0 if opt == 0 or opt == 1 else -1

        for i in range(150):
            if i < 50:
                x=0.1 + np.random.uniform(-0.1, 0.1)
                y=0.5 + np.random.uniform(-0.1, 0.1)
                label = [1, aux, aux]
            elif i < 100:
                x = 0.6+np.random.uniform(-0.1, 0.1)
                y = 0.5 + np.random.uniform(-0.1, 0.1)
                label = [aux, 1, aux]
            else:
                x = 0.35 + np.random.uniform(-0.1, 0.1)
                y = 0.2 + np.random.uniform(-0.1, 0.1)
                label = [aux, aux, 1]
            artificial.append([x, y, label[0], label[1], label[2]])

        self.qtd_neurons = len(label)
        return np.array(artificial)

    def get_samples(self):
        return self.samples

    def get_neurons(self):
        return self.qtd_neurons
