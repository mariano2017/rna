import numpy as np
import matplotlib.pyplot as plt


class Perceptron:

    def __init__(self, data, rate, epochs, option, qtd_neurons, condition):
        self.learning_rate = rate
        self.epochs = epochs
        self.weights = []
        self.samples = data
        self.opt = option
        self.training_set = []
        self.test_set = []
        self.neurons = qtd_neurons
        self.n_attributes = data.shape[1] - qtd_neurons
        self.training_size = 0.8
        self.activationPlot = condition
        self.training_error = []
        self.test_error = []

    def divide(self, data):
        np.random.shuffle(data)
        self.training_set = data[0: int(len(data) * self.training_size)]
        self.test_set = data[int(len(data) * self.training_size):]
    
    def createweights(self, length):
        w = []
        for i in range(self.neurons):
            w.append(np.random.rand(1, length)[0])
        return np.array(w)

    def updateWeights(self, x, erro, y, w):
        error = np.ones((self.n_attributes,self.neurons),dtype=int) * np.asarray(erro)
        derivate = np.ones((self.n_attributes,self.neurons),dtype=int) * np.asarray(self.derivate(y, self.opt))
        return w + self.learning_rate * error.T * derivate.T * x[0:self.n_attributes]

    def step(self, outputs, option):
        v = -1 if(option == 2) else 0
        pos = np.amax(outputs)
        return np.where(outputs == pos, 1, v)

    def derivate(self, y, option=0):
        if (option == 0):
            return 1
        elif (option == 1):
            return y * (1.0 - y)
        elif (option == 2):
            return (1/2)*(1 - y*y)

    def predict(self, x, option=1):
        if(option == 0):
            return self.step(x)
        elif (option == 1):
            return 1.0 / (1.0 + np.exp(-x))
        elif (option == 2):
            return (1 - np.exp(float(-x)))/(1 + np.exp(float(-x)))

    def activation(self, x, w):
        u = np.dot(w, x[0:self.n_attributes])
        u = np.array(u, dtype=np.float64)
        y = self.predict(u, self.opt)
        return np.array(y)

    def iterationError(self, y, x):
        d = self.desired_output(x)
        return d - y

    def desired_output(self, x):
        d = np.array(x[-self.neurons:len(x)])
        return d.astype('int')

    def training(self):
        epochs = 0
        epochError = 1
        trainingErrors = []
        while epochs < self.epochs:
            errors = 0
            error = np.array([])
            for i in range(len(self.training_set)):
                y = self.activation(self.training_set[i], self.weights)
                e = self.iterationError(y, self.training_set[i])
                e2 = self.iterationError(self.limiar(y,self.opt), self.training_set[i])
                self.weights = self.updateWeights(self.training_set[i], e, y, self.weights)
                error = np.append(error, e*e)
                if not np.array_equal(e2, [0, 0, 0]):
                    errors += 1
            epochs += 1

            epochError = error.mean()
            trainingErrors.append(epochError)
            self.training_error.append(trainingErrors)
            if errors == 0:
                break
            
            np.random.shuffle(self.training_set)

    def test(self):
        hits = 0
        testerrors = []
        for i in range(len(self.test_set)):
            y = self.limiar(self.activation(self.test_set[i], self.weights), self.opt)
            d = self.desired_output(self.test_set[i])
            e = d-y
            testerrors.append(e*e)
            if np.array_equal(y,d):
                hits += 1
        self.test_error.append(np.mean(testerrors))
        return hits/len(self.test_set)

    def threshold(self, y, opt):
        if(opt == 0):
            return y
        return self.step(y, opt)
        # return y if opt == 0 else fm.step(y)

    def execute(self, realizations):
        print("### REDE PERCEPTRON ###")
        print("PARÂMETROS: ")
        print("\t Taxa de aprendizagem: ", self.learning_rate)
        print("\t Número máximo de épocas: ", self.epochs)
        print("\t Total de realizações: ", realizations, "\n")
        print("\t Total de atributos: ", self.n_attributes, "\n")
        self.weights = self.createweights(self.n_attributes)
        rates = []
        
        for i in range(realizations):
            print("### REALIZAÇÃO ", (i+1), "###")
            np.random.shuffle(self.samples)  # shuffle entre realizações
            self.weights = self.createweights(self.n_attributes)
            self.divide(self.samples)
            print("### FASE DE TREINAMENTO ###")
            self.training()
            print("### FASE DE TESTE ###")
            rate = self.test()
            print("Taxa: ", rate)
            rates.append(rate)

        print("")
        print("W Final: ", self.weights)
        rates = np.array(rates)
        print("")
        print("Acurácia: ", rates.mean())
        print("Variância: ", rates.var())
        print("Desvio Padrão: ", rates.std())
        if self.activationPlot > 0:
            self.plotError()
            self.plotData2d(self.training_set, "Treinamento");
            self.plotData2d(self.test_set, "Teste")

        print("### FIM PERCEPTRON ###")