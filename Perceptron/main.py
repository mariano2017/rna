import Parameterize as pr
import Perceptron as per

# Caminho da padrão
PATH = "./data/"

# Escolhendo a Database
DATASETNAME = "iris.data"

# Escolher database
# 0 - Datasetname - x atributos;
# 1 - Datasetname - 2 atributos[x1,x2];
# 2 - Datasetname - 2 atributos[x2,x3];
# 3 - Datasetname - 2 atributos[x4,x5];(Caso da Coluna Vertebral)
# 4 - Artificial AND
TYPE = 0

# Escolher derivada
# 0 - Degrau
# 1 - Logistica;
# 2 - Tangente Hiberbolica
TYPE2 = 1

# Nº de realizações
REALIZATIONS = 20

# Taxa de Aprendizagem
RATE = 0.05

# Nº de épocas
EPOCHS = 300

def main():

    parameter = pr.Parameterize(PATH, DATASETNAME, TYPE, TYPE2)
    perceptron = per.Perceptron(parameter.get_samples(), RATE, EPOCHS, TYPE2, parameter.get_neurons(), TYPE)
    perceptron.execute(REALIZATIONS)


if __name__ == "__main__":
    main()
