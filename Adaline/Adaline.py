import numpy as np
import Util as ut
import math

class Adaline:

    def __init__(self, data, realizations, rate, epochs, type):
        self.training_size = 0.8
        self.learning_rate = float(rate)
        self.epochs = epochs
        self.weights = []
        self.option = type
        self.samples = data
        self.training_set = []
        self.test_set = []
        self.test_errors = []
        self.training_errors = []
        self.allw = []
        self.alltrainings = []
        self.alltests = []
        self.execute(realizations)

    def divide(self, data):
        np.random.shuffle(data)
        self.training_set = data[0: int(len(data) * self.training_size)]
        self.test_set = data[int(len(data) * self.training_size):]

    def createWeights(self, length):
        w = np.random.rand(1, length-1)[0]
        return w
        
    def updateWeights(self, x, erro):
        self.weights = self.weights + (self.learning_rate) * (erro) * x[0:len(x) - 1]

    def predict(self, x):
        return np.dot(self.weights, x[0:len(x)-1])

    def iterationError(self, x):
        y = self.predict(x)
        d = self.desired_output(x)
        return d - y

    def desired_output(self, x):
        return x[len(x)-1]

    def training(self):
        epochs = 0
        epochError = 1
        trainingErrors = []

        while epochs < self.epochs:
            error = np.array([])

            for i in self.training_set:
                e = self.iterationError(i)
                self.updateWeights(i, e)
                error = np.append(error, e*e)
            epochs += 1

            epochError = error.mean()
            np.random.shuffle(self.training_set)
            trainingErrors.append(epochError)

        self.allw.append(self.weights)
        self.training_errors.append(trainingErrors)

    def test(self):
        testerrors = []
        for i in self.test_set:
            e = self.iterationError(i)
            testerrors.append(e*e)
        self.test_errors.append(np.mean(testerrors))

    def execute(self, realizations):
        print("### ADALINE ###")
        print("PARÂMETROS: ")
        print("\t Taxa de aprendizagem: ", self.learning_rate)
        print("\t Número máximo de épocas: ", self.epochs)
        print("\t Total de realizações: ", realizations, "\n")
        data = self.samples
        error = []
        sqrt = []
        for i in range(realizations):
            print("### REALIZAÇÃO ", (i+1), "##########")
            self.weights = self.createWeights(data.shape[1])
            np.random.shuffle(data)  # shuffle entre realizações
            
            self.divide(data)
            self.alltrainings.append(self.training_set)
            self.alltests.append(self.training_set)
            self.training()
            self.allw.append(self.weights)
            self.test()

            mse = np.mean(self.test_errors)
            rmse = np.sqrt(mse)
            error.append(mse)
            sqrt.append(rmse)
            
            print("MSE: & ", round(mse, 5))
            print("RMSE: & ", round(rmse, 5))
            print("##########################")

            
        print("MSE médio: ", round((sum(error) / realizations), 5))
        print("RMSE médio: ", round((sum(sqrt) / realizations), 5))
        self.postprocessing()
        print("### FIM ADALINE ###")

    def postprocessing(self):
        auxmin = 1000
        min = 0
        for i in range(len(self.test_errors)):
            if self.test_errors[i] <= auxmin:
                min = i
                auxmin = self.test_errors[i]

        print('Desvio Padrão: ', np.std(self.test_errors))
        print('Plotando dados com o MSE minimo: ')

        ut.plotError(self.training_errors[min])
        ut.plotData2d(self.samples, self.allw[min])
