import Parameterize
import Adaline

TYPE = 2
A = 3
B = 4
C = 2
REALIZATIONS = 20
RATE = 0.01
EPOCHS = 200

def main():

    parameter = Parameterize.Parameterize(TYPE, A, B, C)
    Adaline.Adaline(parameter.getSamples(), REALIZATIONS, RATE, EPOCHS, TYPE)

if __name__ == "__main__":
    main()
