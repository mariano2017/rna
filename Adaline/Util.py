import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D as a3d

def plotError(error):
    plt.plot(range(1, len(error) + 1), error)
    plt.xlabel('Iterações')
    plt.ylabel('Erros')
    plt.show()

def plotData2d(dataset, w):
    y =[]
    for i in dataset:
        y.append(np.dot(w, i[0:len(i)-1]))

    plt.scatter(dataset[:, 1], dataset[:, -1], s=3)
    plt.plot(dataset[:, 1], y, color='r')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.title('ADALINE - Iteração com MSE minimo')
    plt.show()

def plotData3d(dataset, w):
    zModel = []
    for i in dataset:
        zModel.append(np.dot(w, i[0:len(i)-1]))

    zModel = np.asarray(zModel)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    x = list(dataset[:, 1])
    y = list(dataset[:, 2])
    z = list(dataset[:, -1])
    x2,y2 = np.asarray(x), np.asarray(y).T
    ax.plot_trisurf(x2, y2, zModel, linewidth=0.2, antialiased=True,color="Red")
    ax.scatter(x, y, z, c='k', marker='o')
    plt.title('ADALINE - Iteração com MSE minimo')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.show()

def teste(dataset):
    plt.scatter(dataset[:, 1], dataset[:, -1], s=3)
    plt.show()