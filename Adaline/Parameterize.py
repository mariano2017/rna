import numpy as np
import random as rd
import Util as ut 

class Parameterize():

    def __init__(self, option, a, b, c):
        self.samples = []
        if(option == 1):
            self.samples = self.generateData2D(a, b)
        elif option == 2:
            self.samples = self.generateData3D(a, b, c)

    def generateData2D(self, a, b):
        x = np.linspace(0,10,50)
        y = a * x + b
        data = []
        for i in range(len(y)):
            y[i] += np.random.uniform(-1.0, 1.0)

        for i, j in zip(x, y):
            data.append([i, j])
        
        data = np.asarray(data)
        data = self.normalize(data)
        data = self.insertbias(data)

        return np.asarray(data)

    def generateData3D(self, a, b, c):
        data = np.ones((500,3))
        # f(x) = ax + b
        for i in range(500):
            x1=np.random.uniform(1.0,2.0)*10
            x2=np.random.uniform(1.0,2.0)*10
            data[i][0]=x1
            data[i][1]=x2
            data[i][2]=(a * x1 + b * x2 + c + (rd.random() * 10))

        data = self.normalize(data)
        data = self.insertbias(data)
        return np.array(data)

    def insertbias(self, data):
        new = []
        for i in range(len(data)):
            new.append(np.insert(data[i], 0, -1))
        return np.asarray(new)

    def normalize(self, data):
        c = data.shape[1] - 1
        for i in range(data.shape[1]):
            if(i < c):
                max_ = max(data[:, i])
                min_ = min(data[:, i])
                for j in range(data.shape[0]):
                    data[j, i] = (data[j, i] - min_) / (max_ - min_)
        return data

    def getSamples(self):
        return self.samples
