import random

import pandas as pd
import numpy as np

class Parameterize():

    def __init__(self, path, datasetname , type):
        self.path = path + datasetname
        dataset = self.dataselect(type)
        self.samples = self.normalize(dataset)

    def read(self):
        return np.array(pd.read_csv(self.path, delimiter=",", header=None))

    def dataselect(self, opt):
        if opt == 0:
            dataset = self.read()
            dataset = self.class_selection(dataset)
        elif opt == 1:
            dataset = self.read()
            dataset = dataset[:, [0, 1, -1]]
            dataset = self.class_selection(dataset)
        elif opt == 2:
            dataset = self.read()
            dataset = dataset[:, [2, 3, -1]]
            dataset = self.class_selection(dataset)
        elif opt == 3:
            dataset = self.read()
            dataset = dataset[:, [4, 5, -1]]
            dataset = self.class_selection(dataset)
        else:
            dataset = self.artificial_and()
        return dataset

    def class_selection(self, dataset):
        datasetclasses = np.unique(dataset[:, -1])
        size = len(dataset[0]) - 1
        aux = [1, 2, 3]
        for i in range(len(datasetclasses)):
            for j in range(len(dataset)):
                if dataset[j][size] == datasetclasses[i]:
                    dataset[j][size] = aux[i]
        return dataset

    def normalize(self, dataset):
        for i in range(dataset.shape[1]-1):
            max_ = max(dataset[:, i])
            min_ = min(dataset[:, i])
            for j in range(dataset.shape[0]):
                dataset[j, i] = (dataset[j, i] - min_) / (max_ - min_)
        return dataset

    def artificial_and(self):
        artificial = []
        for i in range(400):
            if(i < 100):
                x = 0 + random.uniform(-0.1, 0.1)
                y = 0 + random.uniform(-0.1, 0.1)
                artificial.append([x, y, 0])
            elif(i  >= 100 and  i < 200):
                x = 1 + random.uniform(-0.1, 0.1)
                y = 0 + random.uniform(-0.1, 0.1)
                artificial.append([x, y, 0])
            elif (i >= 200 and i < 300):
                x = 0 + random.uniform(-0.1, 0.1)
                y = 1 + random.uniform(-0.1, 0.1)
                artificial.append([x, y, 0])
            else:
                x = 1 + (random.uniform(-0.1, 0.1))
                y = 1 + (random.uniform(-0.1, 0.1))
                artificial.append([x, y, 1])
        return np.array(artificial)

    def get_samples(self):
        return self.samples
