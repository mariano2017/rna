import Parameterize as pr
import KNN as k

# Caminho da padrão
PATH = "./data/"

# Escolhendo a Database
DATASETNAME = "column_3C.dat"

# Setando o nome do Dataset no plot
NAME = "Iris" if DATASETNAME == "iris.data" else "Coluna Vertebral"
# Escolher Database:
# 0 - Data 4 atributos
# 1 - Data 2 atributos (X1 e X2)
# 2 - Data 2 atributos (X3 e X4)
# 3 - Data 2 atributos (X4 e X5)
TYPE = 1

K = 10

# Setando o título no Plot
NAMETYPE = ""
if TYPE == 1:
    NAMETYPE = str(NAME) + " - X1 e X2"
elif TYPE == 2:
    NAMETYPE = str(NAME) + " - X3 e X4"
elif TYPE == 3:
    NAMETYPE = "Coluna Vertebral - X5 e X6"

# Nº de realizações
REALIZATIONS = 20

def main():

    parameter = pr.Parameterize(PATH, DATASETNAME, TYPE)
    knn = k.KNN(parameter.get_samples(), K, TYPE, NAMETYPE)
    knn.execute(REALIZATIONS)

if __name__ == "__main__":
    main()
