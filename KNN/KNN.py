from collections import Counter
from sklearn.model_selection import KFold
import numpy as np
import math
import matplotlib.pyplot as plt

class KNN:

    def __init__(self, dataset, k, type, nameplot, odd=[3, 5, 7, 9, 11, 13, 15]):
        self.k = k
        self.samples = dataset
        self.train_set = []
        self.printPlot = True if(type != 0) else False
        self.type = nameplot
        self.test_set = []
        self.neighbors = []
        self.n_attributes = len(dataset[0]) - 1
        self.odd = odd
        self.kf = KFold(n_splits=k)
        self.n = len(dataset[0]) - 1
        self.training_size = 0.8

    def calc_distance(self, train_x, test_x):
        dim, soma = len(train_x) - 1, 0
        for i in range(dim):
            soma += math.pow(train_x[i] - test_x[i], 2)
        return float(math.sqrt(soma))

    def divide(self, data):
        np.random.shuffle(data)
        self.train_set = np.array(data[0: int(len(data) * self.training_size)])
        self.test_set = np.array(data[int(len(data) * self.training_size):])

    def order_distance(self):
        self.neighbors = sorted(self.neighbors)

    def best_neighbors(self, k):
        self.neighbors = self.neighbors[0:k]

    def hits(self, x_test, neighbors):
        return 1 if(x_test[-1] == self.select_label(neighbors)) else 0

    def acuracy(self, hits):
        return hits/len(self.test_set)

    def calc_mode(self, values):
        values = values[:, -1]
        data = Counter(values)
        return data.most_common(1)[0][0]

    def select_label(self, neighbors):
        neighbors = np.array(neighbors)
        if self.k > 1:
            label = self.calc_mode(neighbors)
        else:
            label = neighbors[0][1]
        return label

    def train(self, k, train_set, test_set):
        hits = 0
        for test in test_set:
            self.neighbors = []
            for train in train_set:
                self.neighbors.append([self.calc_distance(train, test), train[-1]])
            self.order_distance()
            self.best_neighbors(k)
            hits += self.hits(test, self.neighbors)

        return self.acuracy(hits)

    def crossvalidation(self, dataset):
        acc = []
        for i in self.odd:
            hitratings = []
            for train_index, test_index in self.kf.split(dataset):
                train, test = dataset[train_index], dataset[test_index]
                rate = self.train(i, train, test)
                hitratings.append(np.array(rate))
            acc.append((np.array(hitratings).mean()))

        amax = max(acc)
        return 1 + acc.index(amax)

    def execute(self, realizations):
        print("### START KNN ###")
        print("PARÂMETROS: ")
        print("\t Número de vizinhos: ", self.k)
        rates = []
        for i in range(realizations):
            print("### REALIZAÇÃO ", (i + 1), "###")
            self.divide(self.samples)
            k = self.crossvalidation(self.train_set)
            rate = self.train(k, self.train_set, self.test_set)
            print("Taxa: ", rate)
            rates.append(rate)

        rates = np.array(rates)
        print("")
        print("Acuracia: ", rates.mean())
        print("Variânca: ", rates.var())
        print("Desvio Padrão: ", rates.std())
        self.plot_acuracy(rates)

        if (self.printPlot):
            self.plot(self.train_set)
            self.plot(self.test_set)
        print("### END KNN ###")

    def plot_acuracy(self, rates):
        plt.plot(range(1, len(rates) + 1), rates)
        plt.xlabel('Realizações')
        plt.ylabel('Taxas')
        plt.show()

    def plot(self, dataset):
        dataset1 = []
        dataset2 = []
        dataset3 = []

        #Separando as classes
        for i in range(len(dataset)):
            if(dataset[i][self.n_attributes] == 1):
                dataset1.append(dataset[i])
            elif(dataset[i][self.n_attributes] == 2):
                dataset2.append(dataset[i])
            else:
                dataset3.append(dataset[i])

        # #Setando a superficie
        # for i in np.arange(-0.1, 1.1, 0.01):
        #     for j in np.arange(-0.1, 1.1, 0.01):
        #         if (self.predict([-1, i, j, 1]) > 0):
        #             plt.scatter(i, j, marker=".", c="#00FF7F")
        #         else:
        #             plt.scatter(i, j, marker=".", c="#F08080")

        #Setando Classe 1
        for i in dataset1:
            plt.scatter(i[0], i[1], marker="^", c="#008000")

        #Setando Classe 2
        for k in dataset2:
            plt.scatter(k[0], k[1], marker="*", c="#B22222")

        # Setando Classe 3
        for k in dataset3:
            plt.scatter(k[0], k[1], marker="s", c="#0000FF")

        #Printando Gráfico
        plt.grid(True)
        plt.xlabel("X1")
        plt.ylabel("X2")
        plt.title("Superfície de decisão - " + str(self.type))
        plt.show()


